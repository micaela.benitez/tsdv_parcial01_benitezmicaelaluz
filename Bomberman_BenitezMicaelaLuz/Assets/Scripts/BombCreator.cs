﻿using UnityEngine;
using PlayerScript;
using GameManagerScript;

namespace BombCreatorSript
{
    public class BombCreator : MonoBehaviour
    {
        [Header("Bomb data")]
        public GameObject bombPrefab;
        public Player playerPos;

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
                CreateBombs(playerPos);
        }

        private void CreateBombs(Player player)
        {
            if (GameManager.Get().totalBombs < GameManager.Get().maxBombs)
            {
                Vector3 vecPos = new Vector3(Mathf.Round(playerPos.transform.position.x), Mathf.Round(playerPos.transform.position.y), Mathf.Round(playerPos.transform.position.z));
                GameObject bombGenerator = Instantiate(bombPrefab, vecPos, Quaternion.identity).gameObject;
                bombGenerator.transform.parent = gameObject.transform;
                GameManager.Get().totalBombs++;
            }
        }
    }
}