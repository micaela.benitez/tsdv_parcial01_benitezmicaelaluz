﻿using System;
using System.Collections.Generic;
using UnityEngine;
using GameManagerScript;
using Random = UnityEngine.Random;

/*
Los cubos destructibles y los enemigos rojos y violetas se van a crear en las posiciones que hay libres, en cambio
los enemigos amarillos se van a crear en las posiciones que NO estan libres, o sea, donde estan los cubos, ya que
estos enemigos transpasan todo menos al player y otro enemigos.
Por otro lado, los powerups y la puerta se van a crear debajo de los cubos destructibles.
*/

namespace GameObjectsCreatorScript
{
    public class GameObjectsCreator : MonoBehaviour
    {
        [Header("Game objects data")]
        public GameObject dCubePrefab;
        public GameObject redEnemyPrefab;
        public GameObject violetEnemyPrefab;
        public GameObject yellowEnemyPrefab;
        public GameObject doorPrefab;
        public GameObject livePowerupPrefab;
        public GameObject bombPowerupPrefab;
        public GameObject explosionPowerupPrefab;

        [Header("All positions data")]
        public List<int> positionX;
        public List<int> positionZ;

        private int freePositions;
        private int freePositionsYellowEnemies;

        private List<int> objectsPositions = new List<int>();
        private List<int> objectsUnderDCubesPositions = new List<int>();

        private List<int> dCubesPositions = new List<int>();
        private List<int> redEnemysPositions = new List<int>();
        private List<int> violetEnemysPositions = new List<int>();
        private List<int> yellowEnemysPositions = new List<int>();
        private List<int> doorPosition = new List<int>();
        private List<int> livePowerupPosition = new List<int>(); 
        private List<int> bombPowerupPosition = new List<int>();
        private List<int> explosionPowerupPosition = new List<int>();

        private float initialPosY = 1;
        private float initialPosY2 = 0.5f;

        private int min = 4;
        private int min2 = 1;

        [NonSerialized] public List<Vector3> dCubesPos = new List<Vector3>();   // Esta lista la uso para saber hasta donde tiene que haber particulas de explosion

        private void Start()
        {
            // Calculo todas las posiciones que hay libres
            CalculatedAllFreePositions();

            // Calculo con un random las posiciones a ocupar por los objetos a instanciar
            SetRandomPositionsToObjects(GameManager.Get().totalObjects, min, freePositions + 1, ref objectsPositions);   
            SetRandomPositionsToObjects(GameManager.Get().maxYellowEnemies, min2, freePositionsYellowEnemies + 1, ref yellowEnemysPositions);            

            // Le seteo cada posicion a cada objeto (menos a los enemigos amarillos)
            SetPositionsToObjects();

            // Calculo con un random las posicione a ocupar por los objetos que estan debajo de los cubos destructibles a instanciar
            SetRandomPositionsToObjectsUnderDCubes();

            // Le seteo cada posicion a cada objeto dejado de los cubos destructibles
            SetPositionsToObjectsUnderDCubes();

            // Instancio los objetos
            CreateObjects(dCubesPositions, dCubePrefab, "dCube", initialPosY);
            CreateObjects(redEnemysPositions, redEnemyPrefab, "redEnemy", initialPosY);
            CreateObjects(violetEnemysPositions, violetEnemyPrefab, "violetEnemy", initialPosY);
            CreateObjects(yellowEnemysPositions, yellowEnemyPrefab, "yellowEnemy", initialPosY);
            CreateObjects(doorPosition, doorPrefab, "Door", initialPosY2);
            CreateObjects(livePowerupPosition, livePowerupPrefab, "LivePowerup", initialPosY2);
            CreateObjects(bombPowerupPosition, bombPowerupPrefab, "BombPowerup", initialPosY2);
            CreateObjects(explosionPowerupPosition, explosionPowerupPrefab, "ExplosionPowerup", initialPosY2);
        }

        private void CalculatedAllFreePositions()
        {
            for (int i = 0; i < positionX.Count; i++)
            {
                for (int j = 0; j < positionZ.Count; j++)
                {
                    if (i % 2 == 0 || j % 2 == 0)   // Si la pos en X o Z es par
                        freePositions++;
                    else if (i % 2 != 0 && j % 2 != 0)   // Si la pos en X y Z es impar
                        freePositionsYellowEnemies++;
                }
            }
        }

        private void SetRandomPositionsToObjects(int total, int min, int max, ref List<int> list)
        {
            int numAux = 0;

            for (int i = 0; i < total; i++)
            {
                do
                {
                    numAux = Random.Range(min, max);

                } while (list.Contains(numAux));

                list.Add(numAux);
            }
        }        

        private void SetPositionsToObjects()
        {
            for (int i = 0; i < objectsPositions.Count; i++)
            {
                if (i < GameManager.Get().maxDCubes)
                    dCubesPositions.Add(objectsPositions[i]);
                else if (i < GameManager.Get().maxDCubes + GameManager.Get().maxRedEnemies)
                    redEnemysPositions.Add(objectsPositions[i]);
                else 
                    violetEnemysPositions.Add(objectsPositions[i]);
            }
        }

        private void SetRandomPositionsToObjectsUnderDCubes()
        {
            int numAux = 0;

            for (int i = 0; i < GameManager.Get().totalObjectsUnderCubes; i++)
            {
                do
                {
                    numAux = Random.Range(0, dCubesPositions.Count);

                } while (objectsUnderDCubesPositions.Contains(dCubesPositions[numAux]));

                objectsUnderDCubesPositions.Add(dCubesPositions[numAux]);
            }
        }

        private void SetPositionsToObjectsUnderDCubes()
        {              
            for (int i = 0; i < objectsUnderDCubesPositions.Count; i++)
            {
                if (i < GameManager.Get().totalDoors)
                    doorPosition.Add(objectsUnderDCubesPositions[i]);
                else if (i < GameManager.Get().totalDoors + GameManager.Get().maxLivePowerups)
                    livePowerupPosition.Add(objectsUnderDCubesPositions[i]);
                else if (i < GameManager.Get().totalDoors + GameManager.Get().maxLivePowerups + GameManager.Get().maxBombPowerups)
                    bombPowerupPosition.Add(objectsUnderDCubesPositions[i]);
                else
                    explosionPowerupPosition.Add(objectsUnderDCubesPositions[i]);
            }
        }

        public void CreateObjects(List<int> list, GameObject prefab, string objectType, float posY)
        {
            int objectNumber = 0;

            for (int i = 0; i < freePositions; i++)
            {
                for (int j = 0; j < positionX.Count; j++)
                {
                    for (int t = 0; t < positionZ.Count; t++)
                    {
                        if (((j % 2 == 0 || t % 2 == 0) && objectType != "yellowEnemy") || (j % 2 != 0 && t % 2 != 0 && objectType == "yellowEnemy"))
                        {
                            objectNumber++;
                            if (list.Contains(objectNumber))
                            {
                                Vector3 vecPos = new Vector3(positionX[j], posY, positionZ[t]);
                                GameObject go = Instantiate(prefab, vecPos, Quaternion.identity).gameObject;
                                go.transform.parent = gameObject.transform;

                                if (objectType == "dCube")
                                    dCubesPos.Add(vecPos);
                            }
                        }
                    }
                }
            }
        }
    }
}