﻿using UnityEngine;

namespace ParticleTimerScript
{
    public class ParticleTimer : MonoBehaviour
    {
        public float timeToDie = 1;
        private float timer = 0;

        private void Update()
        {
            timer += Time.deltaTime;

            if (timer > timeToDie)
                Destroy(gameObject);
        }
    }
}