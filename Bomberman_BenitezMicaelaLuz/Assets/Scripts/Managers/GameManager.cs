﻿using System;
using UnityEngine;
using LoaderManagerScript;
using MonoBehaviourSingletonScript;

namespace GameManagerScript
{  
    public class GameManager : MonoBehaviourSingleton<GameManager>
    {
        public static event Action OnDoorOpen;
        [NonSerialized] public bool doorOpen;

        [Header("Player data")]
        public int maxLives = 2;
        [NonSerialized] public int totalLives;

        [Header("Bomb data")]
        public int maxBombsPerTime = 1;   // Cantidad inicial de bombas a la vez
        public float bombTimer = 5;
        public int initialDistanceExplosion = 1;   // Distancia de la explosion inicial
        [NonSerialized] public int maxBombs;   // Cantidad de bombas a al vez
        [NonSerialized] public int distanceExplosion;   // Distancia de la explosion
        [NonSerialized] public int totalBombs;   // Cantidad de bombas activas

        [Header("Destroyable Cubes data")]
        public int maxDCubes = 10;   // Cubos destructibles que se quiere que hayan en la escena
        [NonSerialized] public int totalDCubes;   // Cubos destructibles activos

        [Header("Red Enemies data")]
        public int maxRedEnemies = 3;
        public int redEnemiesPoints = 10;
        [NonSerialized] public int totalRedEnemies;

        [Header("Violet Enemies data")]
        public int maxVioletEnemies = 3;
        public int violetEnemiesPoints = 20;
        [NonSerialized] public int totalVioletEnemies;

        [Header("Yellow Enemies data")]
        public int maxYellowEnemies = 3;
        public int yellowEnemiesPoints = 30;
        [NonSerialized] public int totalYellowEnemies;

        [Header("Total powerups data")]
        public int maxLivePowerups = 1;
        public int maxBombPowerups = 2;
        public int maxExplosionPowerups = 2;

        [NonSerialized] public int totalDoors = 1;

        [NonSerialized] public int totalEnemies;
        [NonSerialized] public int totalObjects;
        [NonSerialized] public int totalObjectsUnderCubes;

        [NonSerialized] public float timer;
        [NonSerialized] public float score;

        public void InitData()
        {
            doorOpen = false;
            timer = 0;
            score = 0;
            totalLives = maxLives;
            totalBombs = 0;
            totalDCubes = maxDCubes;
            totalRedEnemies = maxRedEnemies;
            totalVioletEnemies = maxVioletEnemies;
            totalYellowEnemies = maxYellowEnemies;
            maxBombs = maxBombsPerTime;
            distanceExplosion = initialDistanceExplosion;

            totalEnemies = maxRedEnemies + maxVioletEnemies + maxYellowEnemies;
            totalObjects = maxDCubes + maxRedEnemies + maxVioletEnemies;
            totalObjectsUnderCubes = maxLivePowerups + maxBombPowerups + maxExplosionPowerups + totalDoors;
        }

        public void AskIfThePlayerLost()
        {
            if (totalLives == 0)
                LoaderManager.Get().LoadScene("Result");
        }

        public void AskIfTheDoorIsOpen()
        {
            if (totalEnemies == 0)
            {
                DoorOpen();
                doorOpen = true;
            }
        }

        private void DoorOpen()
        {
            OnDoorOpen?.Invoke();
        }
    }
}