﻿using UnityEngine;
using TMPro;
using LoaderManagerScript;
using GameManagerScript;

namespace UIGameScript
{
    public class UIGame : MonoBehaviour
    {
        public TMP_Text time;
        public TMP_Text score;
        public TMP_Text lives;
        public TMP_Text enemies;
        public TMP_Text bombs;
        public TMP_Text explosion;

        public void Update()
        {
            time.text = "" + GameManager.Get().timer.ToString("F");
            score.text = "" + GameManager.Get().score;
            lives.text = "" + GameManager.Get().totalLives;
            enemies.text = "" + GameManager.Get().totalEnemies;
            bombs.text = "" + GameManager.Get().totalBombs;
            explosion.text = "" + GameManager.Get().distanceExplosion;
        }

        public void LoadMenuScene()
        {
            LoaderManager.Get().LoadScene("Menu");
        }

        public void ExitGame()
        {
            Application.Quit();
        }
    }
}