﻿using UnityEngine;
using LoaderManagerScript;

namespace UIMenuScript
{
    public class UIMenu : MonoBehaviour
    {
        public void LoadGameScene()
        {
            LoaderManager.Get().LoadScene("Game");
        }

        public void LoadInstructionsScene()
        {
            LoaderManager.Get().LoadScene("Instructions");
        }

        public void LoadCreditsScene()
        {
            LoaderManager.Get().LoadScene("Credits");
        }

        public void ExitGame()
        {
            Application.Quit();
        }
    }
}