﻿using UnityEngine;
using TMPro;
using LoaderManagerScript;
using GameManagerScript;

namespace UIResultScript
{
    public class UIResult : MonoBehaviour
    {
        public TMP_Text result;
        public TMP_Text time;
        public TMP_Text score;
        public TMP_Text lives;

        private void Start()
        {
            if (GameManager.Get().totalLives > 0)
                result.text = "WINNER";
            else
                result.text = "LOSER";
        }

        private void Update()
        {
            time.text = "" + GameManager.Get().timer.ToString("F");
            score.text = "" + GameManager.Get().score;
            lives.text = "" + GameManager.Get().totalLives;
        }

        public void LoadMenuScene()
        {
            LoaderManager.Get().LoadScene("Menu");
        }

        public void LoadGameScene()
        {
            LoaderManager.Get().LoadScene("Game");
        }
    }
}