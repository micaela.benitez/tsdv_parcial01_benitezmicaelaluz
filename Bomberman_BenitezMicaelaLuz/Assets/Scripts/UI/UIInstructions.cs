﻿using UnityEngine;
using LoaderManagerScript;

namespace UIInstructionsScript
{
    public class UIInstructions : MonoBehaviour
    {
        public void LoadMenuScene()
        {
            LoaderManager.Get().LoadScene("Menu");
        }

        public void LoadGameScene()
        {
            LoaderManager.Get().LoadScene("Game");
        }
    }
}