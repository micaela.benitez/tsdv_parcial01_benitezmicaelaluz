﻿using UnityEngine;
using LoaderManagerScript;

namespace UICreditScript
{
    public class UICredits : MonoBehaviour
    {
        public void LoadMenuScene()
        {
            LoaderManager.Get().LoadScene("Menu");
        }
    }
}