﻿using System.Collections.Generic;
using UnityEngine;
using GameManagerScript;
using Random = UnityEngine.Random;

namespace EnemyScript
{
    public class Enemy : MonoBehaviour
    {
        [Header("Enemy data")]
        public float speed = 2;

        private float rayDistance = 0.5f;

        private List<string> directions = new List<string>();
        private string actualDirection;

        private float timer = 0;
        private float timePerChangeDirection = 5;

        private void Start()
        {
            AddPossibleDirections(transform.forward, "arriba");
            AddPossibleDirections(-transform.forward, "abajo");
            AddPossibleDirections(transform.right, "derecha");
            AddPossibleDirections(-transform.right, "izquierda");

            CalculateDirection();
        }

        private void Update()
        {
            timer += Time.deltaTime;            

            transform.rotation = Quaternion.Euler(0,0,0);

            Debug.DrawRay(transform.position, transform.forward * rayDistance, Color.black);
            Debug.DrawRay(transform.position, -transform.forward * rayDistance, Color.black);
            Debug.DrawRay(transform.position, transform.right * rayDistance, Color.black);
            Debug.DrawRay(transform.position, -transform.right * rayDistance, Color.black);            

            if (timer > timePerChangeDirection && Mathf.Round(transform.position.x) % 2 != 0 && Mathf.Round(transform.position.z) % 2 != 0)
            {
                transform.position = new Vector3(Mathf.Round(transform.position.x), transform.position.y, Mathf.Round(transform.position.z));

                if (actualDirection == "derecha" || actualDirection == "izquierda")
                {
                    AddPossibleDirections(transform.forward, "arriba");
                    AddPossibleDirections(-transform.forward, "abajo");
                }
                else
                {
                    AddPossibleDirections(transform.right, "derecha");
                    AddPossibleDirections(-transform.right, "izquierda");
                }

                if (directions.Count > 0)
                    CalculateDirection();

                timer = 0;
            }

            Movement();
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (actualDirection == "arriba")
                actualDirection = "abajo";
            else if (actualDirection == "abajo")
                actualDirection = "arriba";
            else if (actualDirection == "derecha")
                actualDirection = "izquierda";
            else if (actualDirection == "izquierda")
                actualDirection = "derecha";

            if (collision.transform.tag == "Player")
            {
                GameManager.Get().totalLives--;
                GameManager.Get().AskIfThePlayerLost();
            }
        }

        private void AddPossibleDirections(Vector3 direction, string possibleDirection)
        {
            RaycastHit hit;

            if (!Physics.Raycast(transform.position, direction, out hit, rayDistance))
                directions.Add(possibleDirection);
        }

        private void CalculateDirection()
        {
            int aux = Random.Range(0, directions.Count);

            if (directions[aux] == "arriba")
                actualDirection = "arriba";
            else if (directions[aux] == "abajo")
                actualDirection = "abajo";
            else if (directions[aux] == "derecha")
                actualDirection = "derecha";
            else if (directions[aux] == "izquierda")
                actualDirection = "izquierda";

            directions.Clear();
        }

        private void Movement()
        {
            if (actualDirection == "arriba")
                transform.position += transform.forward * speed * Time.deltaTime;
            else if (actualDirection == "abajo")
                transform.position += -transform.forward * speed * Time.deltaTime;
            else if (actualDirection == "derecha")
                transform.position += transform.right * speed * Time.deltaTime;
            else if (actualDirection == "izquierda")
                transform.position += -transform.right * speed * Time.deltaTime;

            // Me aseguro que los enemigos se mantengan en el medio
            if (actualDirection == "arriba" || actualDirection == "abajo")
                transform.position = new Vector3(Mathf.Round(transform.position.x), transform.position.y, transform.position.z);
            else
                transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Round(transform.position.z));
        }
    }
}