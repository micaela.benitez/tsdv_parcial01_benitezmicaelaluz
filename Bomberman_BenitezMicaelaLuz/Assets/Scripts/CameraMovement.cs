﻿using UnityEngine;

namespace CameraMovementScript
{
    public class CameraMovement : MonoBehaviour
    {
        public Transform player;
        public Vector3 playerDistance;
        public float rotationX;
        public float rotationY;
        public float rotationZ;

        private void Start()
        {
            transform.rotation = Quaternion.Euler(rotationX, rotationY, rotationZ);
        }

        private void Update()
        {
            transform.position = new Vector3(player.position.x - playerDistance.x, playerDistance.y, player.position.z - playerDistance.z);
        }
    }
}