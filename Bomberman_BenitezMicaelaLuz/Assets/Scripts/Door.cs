﻿using UnityEngine;
using GameManagerScript;

namespace DoorScript
{
    public class Door : MonoBehaviour
    {
        private Renderer render;

        private void Awake()
        {
            render = GetComponent<Renderer>();
        }

        private void Start()
        {
            GameManager.OnDoorOpen += DoorOpen;
        }

        private void DoorOpen()
        {
            render.material.SetColor("_Color", Color.white);
        }

        private void OnDestroy()
        {
            GameManager.OnDoorOpen -= DoorOpen;
        }
    }
}