﻿using System;
using UnityEngine;
using GameManagerScript;
using PlayerScript;
using GameObjectsCreatorScript;

namespace BombScript
{
    public class Bomb : MonoBehaviour
    {
        public static event Action OnResetPlayerPos;
        public static event Action OnPlayerAway;

        public ParticleSystem explosion;

        private Player playerPos;
        private GameObjectsCreator go;

        private float time;
        private float rayDistance;
        private float size;

        private void Awake()
        {
            size = transform.lossyScale.x;
            rayDistance = GameManager.Get().distanceExplosion + size / 2;

            playerPos = FindObjectOfType<Player>();
            go = FindObjectOfType<GameObjectsCreator>();
        }

        private void Update()
        {
            Debug.DrawRay(transform.position, transform.forward * rayDistance, Color.black);
            Debug.DrawRay(transform.position, -transform.forward * rayDistance, Color.black);
            Debug.DrawRay(transform.position, transform.right * rayDistance, Color.black);
            Debug.DrawRay(transform.position, -transform.right * rayDistance, Color.black);

            time += Time.deltaTime;

            if (time > GameManager.Get().bombTimer)   // Si la bomba llega al tiempo predeterminado explota
            {
                // Efecto explosion
                ExplosionEffect(90, "Arriba");
                ExplosionEffect(90, "Abajo");
                ExplosionEffect(0, "Derecha");
                ExplosionEffect(0, "Izquierda");

                // Colisiones explosion
                CollisionExplosionBomb(transform.forward);
                CollisionExplosionBomb(-transform.forward);
                CollisionExplosionBomb(transform.right);
                CollisionExplosionBomb(-transform.right);
                PlayerAboveBomb();

                GameManager.Get().totalBombs--;
                Destroy(gameObject);
            }

            if (Mathf.Round(playerPos.transform.position.x) != Mathf.Round(transform.position.x) ||
                Mathf.Round(playerPos.transform.position.z) != Mathf.Round(transform.position.z))
                PlayerAway();
        }

        private void ExplosionEffect(float rotY, string dir)
        {
            for (int i = 1; i <= GameManager.Get().distanceExplosion; i++)
            {
                Quaternion newRot = Quaternion.Euler(0, rotY, 0);
                Vector3 vecPos = new Vector3(0, 0, 0);

                if (dir == "Arriba")
                    vecPos = new Vector3(transform.position.x, transform.position.y, transform.position.z + (1 * i));
                else if (dir == "Abajo")
                    vecPos = new Vector3(transform.position.x, transform.position.y, transform.position.z - (1 * i));
                else if (dir == "Derecha")
                    vecPos = new Vector3(transform.position.x + (1 * i), transform.position.y, transform.position.z);
                else if (dir == "Izquierda")
                    vecPos = new Vector3(transform.position.x - (1 * i), transform.position.y, transform.position.z);
        
                // Si es esa pos hay cubo estatico o la pared, para esa direccion no se hace el efecto y tampoco sigue
                if ((vecPos.x % 2 == 0 && vecPos.z % 2 == 0) || (vecPos.x > 9 || vecPos.x < -9 || vecPos.z > 9 || vecPos.z < -9))
                    i = GameManager.Get().distanceExplosion;
                else
                    Instantiate(explosion, vecPos, newRot);
        
                // Chequeo donde hay cubos destructibles
                for (int j = 0; j < go.dCubesPos.Count; j++)
                {
                    if (vecPos == go.dCubesPos[j])   // Si en esa pos hay un cubo destructible, para esa direccion no sigue el efecto
                    {
                        i = GameManager.Get().distanceExplosion;
                        go.dCubesPos.Remove(vecPos);
                    }
                }
            }
        }

        private void CollisionExplosionBomb(Vector3 direction)
        {
            RaycastHit hit;
            int maxCubesDestroyed = 1;
            int totalCubesDestroyed = 0;            

            if (Physics.Raycast(transform.position, direction, out hit, rayDistance))
            {
                // Si colisionan con el player le restan 1 punto de vida
                if (hit.transform.gameObject.tag == "Player")
                    IfPlayerDied();

                // Si colisionan con un cubo destructible, este se rompe
                if (hit.transform.gameObject.tag == "DCube" && totalCubesDestroyed < maxCubesDestroyed)   
                {

                    totalCubesDestroyed++;
                    Destroy(hit.transform.gameObject); 
                    GameManager.Get().totalDCubes--;
                }

                // Si colisionan con un enemigo, lo mata
                if (hit.transform.gameObject.tag == "RedEnemy" ||   
                    hit.transform.gameObject.tag == "VioletEnemy" || 
                    hit.transform.gameObject.tag == "YellowEnemy")   
                {
                    Destroy(hit.transform.gameObject);
                    GameManager.Get().totalEnemies--;
                    GameManager.Get().AskIfTheDoorIsOpen();

                    if (hit.transform.gameObject.tag == "RedEnemy")
                        GameManager.Get().score += GameManager.Get().redEnemiesPoints;
                    else if (hit.transform.gameObject.tag == "VioletEnemy")
                        GameManager.Get().score += GameManager.Get().violetEnemiesPoints;
                    else
                        GameManager.Get().score += GameManager.Get().yellowEnemiesPoints;
                }
            }
        }

        private void PlayerAboveBomb()
        {
            if (Mathf.Round(playerPos.transform.position.x) == Mathf.Round(transform.position.x) &&
                Mathf.Round(playerPos.transform.position.z) == Mathf.Round(transform.position.z))
                IfPlayerDied();
        }

        private void IfPlayerDied()
        {
            GameManager.Get().totalLives--;
            GameManager.Get().AskIfThePlayerLost();
            ResetPlayerPosition();
        }

        private void ResetPlayerPosition()
        {
            OnResetPlayerPos?.Invoke();
        }

        private void PlayerAway()
        {
            OnPlayerAway?.Invoke();
        }
    }
}