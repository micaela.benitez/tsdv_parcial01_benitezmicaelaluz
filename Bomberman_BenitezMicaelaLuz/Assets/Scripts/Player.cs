﻿using UnityEngine;
using GameManagerScript;
using BombScript;
using LoaderManagerScript;

namespace PlayerScript
{
    public class Player : MonoBehaviour
    {
        [Header("Player data")]
        public float speed;

        private float scaleX;
        private float scaleZ;

        private float rayDistance = 0.5f;

        private int initialPosX = -9;
        private int initialPosZ = -9;

        private Rigidbody rig;

        private void Awake()
        {
            GameManager.Get().InitData();
            rig = GetComponent<Rigidbody>();
        }

        private void Start()
        {
            scaleX = transform.lossyScale.x;
            scaleZ = transform.lossyScale.z;

            Bomb.OnResetPlayerPos += ResetPlayerPosition;
            Bomb.OnPlayerAway += PlayerIsNotKinematic;
        }

        private void Update()
        {
            GameManager.Get().timer += Time.deltaTime;

            transform.rotation = Quaternion.Euler(0, 0, 0);
            transform.position = new Vector3(transform.position.x, 1, transform.position.z);

            // Variables de movimiento
            float hor = Input.GetAxis("Horizontal");
            float ver = Input.GetAxis("Vertical");

            // Variables que calculan la posicion del personaje movida para cada raycast
            Vector3 rightPos = new Vector3(transform.position.x + scaleX / 2, transform.position.y, transform.position.z);
            Vector3 leftPos = new Vector3(transform.position.x - scaleX / 2, transform.position.y, transform.position.z);
            Vector3 upPos = new Vector3(transform.position.x, transform.position.y, transform.position.z + scaleZ / 2);
            Vector3 downPos = new Vector3(transform.position.x, transform.position.y, transform.position.z - scaleZ / 2);            

            // Dibujo cada raycast
            PlayerDrawRaycast(rightPos, leftPos, upPos, downPos);

            // Verifico si alguno raycast colisiona con un objeto
            PlayerRaycastCollision(rightPos, transform.forward, ref ver);
            PlayerRaycastCollision(leftPos, transform.forward, ref ver);

            PlayerRaycastCollision(rightPos, -transform.forward, ref ver);
            PlayerRaycastCollision(leftPos, -transform.forward, ref ver);

            PlayerRaycastCollision(upPos, transform.right, ref hor);
            PlayerRaycastCollision(downPos, transform.right, ref hor);

            PlayerRaycastCollision(upPos, -transform.right, ref hor);
            PlayerRaycastCollision(downPos, -transform.right, ref hor);

            // Chequea si el personaje puede doblar, si es asi, lo centra y dobla
            CheckIfTheCharacterCanDouble(hor, upPos, downPos, transform.right, "Horizontal", transform.position.z);
            CheckIfTheCharacterCanDouble(ver, rightPos, leftPos, transform.forward, "Vertical", transform.position.x);

            // Actualizo la pos del player
            Vector3 direction = new Vector3(hor, 0, ver);
            transform.position += direction * speed * Time.deltaTime;

            if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
                rig.isKinematic = true;
        }

        private void PlayerDrawRaycast(Vector3 rightPos, Vector3 leftPos, Vector3 upPos, Vector3 downPos)
        {
            Debug.DrawRay(rightPos, transform.forward * rayDistance, Color.black);    // Raycast de adelate derecho
            Debug.DrawRay(rightPos, -transform.forward * rayDistance, Color.black);   // Raycast de atras derecho

            Debug.DrawRay(leftPos, transform.forward * rayDistance, Color.red);    // Raycast de adelate izquierdo
            Debug.DrawRay(leftPos, -transform.forward * rayDistance, Color.red);   // Raycast de atras izquiero

            Debug.DrawRay(upPos, transform.right * rayDistance, Color.green);      // Raycast derecho de arriba
            Debug.DrawRay(upPos, -transform.right * rayDistance, Color.green);     // Raycast izquierdo de arriba

            Debug.DrawRay(downPos, transform.right * rayDistance, Color.blue);      // Raycast derecho de abajo
            Debug.DrawRay(downPos, -transform.right * rayDistance, Color.blue);     // Raycast izquierdo de abajo
        }

        private void PlayerRaycastCollision(Vector3 origin, Vector3 direction, ref float movement)
        {
            RaycastHit hit;

            if (Physics.Raycast(origin, direction, out hit, rayDistance))
            {
                if (direction == transform.forward || direction == transform.right)
                {
                    if (movement > 0)
                        movement = 0;
                }
                else
                {
                    if (movement < 0)
                        movement = 0;
                }
            }
        }

        private void CheckIfTheCharacterCanDouble(float dir, Vector3 origin1, Vector3 origin2, Vector3 direction, string direc, float pos)
        {
            RaycastHit hit;

            // Centro al player si es que donde quiero doblar y no hay un cubo destructible
            if ((dir > 0 && (!Physics.Raycast(origin1, direction, out hit, rayDistance) || !Physics.Raycast(origin2, direction, out hit, rayDistance))) ||
                (dir < 0 && (!Physics.Raycast(origin1, -direction, out hit, rayDistance) || !Physics.Raycast(origin2, -direction, out hit, rayDistance))))
                CenterThePlayer(direc, pos);
        }

        private void CenterThePlayer(string direction, float pos)
        {
            float roundedPos = 0;
            float aux = 0;

            if (Input.GetButton(direction))
            {
                roundedPos = Mathf.Round(pos);
                if (roundedPos % 2 != 0)   // Si el numero redondeado de la pos actual es impar, significa que no hay cubo y puedo doblar
                {
                    if (pos == transform.position.z)
                    {
                        aux = Mathf.Lerp(pos, roundedPos, 0.3f);
                        transform.position = new Vector3(transform.position.x, transform.position.y, aux);
                    }
                    if (pos == transform.position.x)
                    {
                        aux = Mathf.Lerp(pos, roundedPos, 0.3f);
                        transform.position = new Vector3(aux, transform.position.y, transform.position.z);
                    }
                }
            }
        }

        private void ResetPlayerPosition()
        {
            transform.position = new Vector3(initialPosX, transform.position.y, initialPosZ);
        }

        private void PlayerIsNotKinematic()
        {
            rig.isKinematic = false;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.transform.gameObject.tag == "Door" && GameManager.Get().doorOpen)
            {
                LoaderManager.Get().LoadScene("Result");
            }
            else if (other.transform.gameObject.tag != "Door")
            {
                if (other.transform.gameObject.tag == "LivePowerup")
                    GameManager.Get().totalLives++;
                else if (other.transform.gameObject.tag == "BombPowerup")
                    GameManager.Get().maxBombs++;
                else if (other.transform.gameObject.tag == "ExplosionPowerup")
                    GameManager.Get().distanceExplosion++;

                Destroy(other.transform.gameObject);
            }
        }

        private void OnDestroy()
        {
            Bomb.OnResetPlayerPos -= ResetPlayerPosition;
            Bomb.OnPlayerAway -= PlayerIsNotKinematic;
        }
    }
}